import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {NotificationDto} from '@app/_models/dto';
import {NotificationService} from '@app/_services/notification.service';

@Component({
  selector: 'app-served-notifications',
  templateUrl: './served-notifications.component.html',
  styleUrls: ['./served-notifications.component.less']
})
export class ServedNotificationsComponent implements OnInit {

  nameColumn: string[] = ['id', 'desc', 'incidentType', 'incidentTypeDesc', 'more'];
  notificationDataSource: MatTableDataSource<NotificationDto>;
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.notificationDataSource = new MatTableDataSource<NotificationDto>([]);
    this.getNotifications();
  }

  isListContainingNotification() {
    return this.notificationDataSource.data.length !== 0;
  }

  getNotifications() {
    this.notificationService.getAllServicedNotification().subscribe(
        resp => {
          console.log(resp);
          this.notificationDataSource = new MatTableDataSource<NotificationDto>(resp);
          this.notificationDataSource.paginator = this.paginator;
        });


  }

}
