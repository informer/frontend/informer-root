import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServedNotificationsComponent } from './served-notifications.component';

describe('ServedNotificationsComponent', () => {
  let component: ServedNotificationsComponent;
  let fixture: ComponentFixture<ServedNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServedNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServedNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
