﻿import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';

import {AuthenticationService} from '@app/_services';
import {UserForm} from '@app/_models/userform';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.returnUrl = '/';
    }

    get f() {
        return this.loginForm.controls;
    }

    get isAdmin() {
        return localStorage.getItem('ROLE') === 'ROLE_ADMIN';
    }

    get isDispatcher() {
        return localStorage.getItem('ROLE') === 'ROLE_DISPATCHER';
    }

    get isRegularUser() {
        return localStorage.getItem('ROLE') === 'ROLE_REGULAR';
    }

    onSubmit() {
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        const user = new UserForm(this.f.email.value, this.f.password.value);
        this.authenticationService.login(user)
            .pipe()
            .subscribe(
                data => {
                    console.log(data);
                    if (this.isRegularUser) {
                        location.replace('/');
                    }
                    if (this.isDispatcher) {
                        location.replace('/notification-list');
                    }
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }
}
