import {Component, OnInit} from '@angular/core';
import {
    AreaDto,
    CoordsDto,
    IncidentNotificationForm,
    IncidentTypeDto,
    PublicServiceAddForm, PublicServiceDto,
    PublicServiceUnitAddForm
} from '@app/_models/dto';
import {Marker} from '@app/map/map.component';
import {FormControl} from '@angular/forms';
import {PublicServicesService} from '@app/_services/publicservices.service';
import swal from 'sweetalert2';
import {Router} from '@angular/router';


declare let google: any;

@Component({
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.less']
})
export class ServicesComponent implements OnInit {
    publicServiceAddForm: PublicServiceAddForm = new PublicServiceAddForm();
    publicServiceUnitAddForm: PublicServiceUnitAddForm = new PublicServiceUnitAddForm();
    private coords: CoordsDto;
    private mapCenter: CoordsDto;
    private address: string;
    private mapType = 'roadmap';
    private zoom = 13;
    private marker: Marker;
    private addressResult: any;
    servicesListDto: any;
    services = new FormControl();
    emailPattern = '[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]' +
        '{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*';
    phonePattern = '\\+48\\d{9}';

    constructor(private publicServicesService: PublicServicesService, private router: Router) {
    }

    ngOnInit() {
        this.setCurrentPosition();
        this.loadAddress();
        this.getService();
    }

    getService() {
        this.publicServicesService.getServices().subscribe(resp => {
            this.servicesListDto = resp;
            return resp;
        });
    }

    public loadAddress() {
        if (this.address === undefined) {
            this.getAddress();
            console.log(this.address);
        }
    }

    public addMarker(latitude: number, longitude: number) {
        console.log(latitude);
        console.log(longitude);
        this.coords = {
            latitude,
            longitude
        };
        this.marker = {
            lat: latitude,
            lng: longitude,
            label: '',
            iconUrl: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        };
        this.getAddress();
    }

    async setCurrentPosition() {
        if (navigator.geolocation) {
            await navigator.geolocation.getCurrentPosition(position => {
                const {longitude, latitude} = position.coords;
                this.coords = {
                    latitude,
                    longitude
                };
                this.mapCenter = {
                    latitude,
                    longitude
                };
                this.marker = {
                    lat: latitude,
                    lng: longitude,
                    label: '',
                    iconUrl: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };
            });
        } else {
            alert('Geolocation is not supported by this browser, please use google chrome.');
        }
    }

    async getAddress(): Promise<any> {
        if (navigator.geolocation && this.coords.longitude && this.coords.latitude) {
            const geocoder = new google.maps.Geocoder();
            const latlng = new google.maps.LatLng(this.coords.latitude, this.coords.longitude);
            const request = {latLng: latlng};
            geocoder.geocode(request, (results, status) => {
                if (status === google.maps.GeocoderStatus.OK) {
                    while (results == null) {
                    }
                    const result = results[0];
                    this.addressResult = result;
                    this.address = result.formatted_address;
                    return result.formatted_address;
                }
            });
        }
    }

    addPublicService() {
        this.publicServicesService.addPublicService(this.publicServiceAddForm).subscribe(resp => {
                swal.fire('Usługa dodana', 'Usługa została dodana', 'success').then(r => {
                });
                this.getService();
            }
        );
    }

    resolveAddress() {
        this.publicServiceUnitAddForm.address = {
            city: this.addressResult.address_components[4].long_name,
            street: this.addressResult.address_components[1].long_name,
            buildingNumber: this.addressResult.address_components[0].long_name,
            coords: this.coords,
            areaId: 1
        };
    }

    public getServiceFromEvent(service: PublicServiceDto, event: any) {
        if (event.isUserInput) {
            this.publicServiceUnitAddForm.publicServiceId = service.id;
        }
    }

    addPublicServiceUnit() {
        console.log(this.publicServiceUnitAddForm);
        if (this.address) {
            this.resolveAddress();
        }
        this.publicServicesService.addPublicServiceUnit(this.publicServiceUnitAddForm).subscribe(resp => {
                swal.fire('Placówka dodana', 'Placówka została dodana', 'success').then(r => {
                });
                this.publicServiceUnitAddForm = new PublicServiceUnitAddForm();
            }
        );
    }
}
