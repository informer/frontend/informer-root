﻿import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin';
import { LoginComponent } from './login';
import { AuthGuard } from './_helpers';
import { Role } from './_models';
import {RegistrationComponent} from '@app/registration/registration.component';
import {MapComponent} from '@app/map/map.component';
import {NotificationsComponent} from '@app/notifications/notifications.component';
import {NotifyComponent} from '@app/notify/notify.component';
import {NotificationListComponent} from '@app/notification-list/notification-list.component';
import {ServicesComponent} from '@app/services/services.component';
import {InctypesComponent} from '@app/inctypes/inctypes.component';
import {NotificationComponent} from '@app/notification/notification.component';
import {ServedNotificationsComponent} from '@app/served-notifications/served-notifications.component';

const routes: Routes = [

    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
         data: { roles: [Role.Dispatcher] }
    },
    {
        path: 'map',
        component: MapComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.Regular] }
    },
    {
        path: '',
        component: NotificationsComponent,
        canActivate: [AuthGuard],
         data: { roles: [Role.Regular] }
    },
    {
        path: 'notification-served',
        component: ServedNotificationsComponent,
        canActivate: [AuthGuard],
         data: { roles: [Role.Dispatcher] }
    },
    {
        path: 'notification-list',
        component: NotificationListComponent,
        canActivate: [AuthGuard],
         data: { roles: [Role.Dispatcher] }
    },
    {
        path: 'notification-list/:idNotification',
        component: NotificationComponent,
        canActivate: [AuthGuard],
         data: { roles: [Role.Dispatcher] }
    },
    {
        path: 'incident-types',
        component: InctypesComponent,
        canActivate: [AuthGuard],
         data: { roles: [Role.Dispatcher] }
    },
    {
        path: 'services',
        component: ServicesComponent,
        canActivate: [AuthGuard],
         data: { roles: [Role.Dispatcher] }
    },
    {
        path: 'notify',
        component: NotifyComponent,
        canActivate: [AuthGuard],
         data: { roles: [Role.Regular] }
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegistrationComponent
    },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);
