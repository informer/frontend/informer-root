import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {PublicServicesService} from '@app/_services/publicservices.service';
import {AreaService} from '@app/_services/area.service';
import {AreaDto, CoordsDto, ServiceUnitDto} from '@app/_models/dto';


declare let google: any;

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.less']
})
export class MapComponent implements OnInit {
    location: Location;
    selectedMarker: Marker;
    address: string;
    origin: any = null;
    destination: any = null;
    error: any;
    loaded = false;
    distance: number;
    servicesListDto: any;
    services = new FormControl();
    areas: Array<AreaDto>;
    serviceUnits: Array<ServiceUnitDto> = new Array<ServiceUnitDto>();
    serviceLoaded = false;
    currentArea: AreaDto;
    defaultCords: CoordsDto;


    constructor(private publicServicesService: PublicServicesService, private areaService: AreaService) {
    }

    ngOnInit() {
        this.servicesListDto = this.getService();
        // @ts-ignore
        this.areas = this.getAreas();
        this.setCurrentPosition();
        this.selectedMarker = this.location.markersToRender[0];

    }

    onSelectionChanged(event) {
        console.log(event.option.value);
    }

    show() {
        this.loaded = true;
        console.log(this.servicesListDto);
        // console.log(this.areas);
        // console.log(this.getCurrentArea());
        console.log(this.serviceUnits);
        console.log(this.location);
        this.getAddress(this.location.coords.latitude, this.location.coords.longitude);
        // console.log(this.location);
    }

    getService() {
        this.publicServicesService.getServices().subscribe(resp => {
            this.servicesListDto = resp;
            return resp;
        });
    }

    getServiceUnits(service: any, event: any) {
        this.clearRoute();
        if (event.isUserInput) {
            this.serviceUnits = new Array<ServiceUnitDto>();
            const tmp = this.location.markersToRender[0];
            this.location.markersToRender = this.location.markersToRender.slice(0, 0);
            this.location.markersToRender.push(tmp);
            this.publicServicesService.getServiceUnits(1, service.id).subscribe(resp => {
                this.serviceUnits = resp;
                this.serviceUnits.forEach(r => {
                    this.location.markersToRender.push(
                        {
                            lat: Number(r.address.coords.latitude),
                            lng: Number(r.address.coords.longitude),
                            label: r.name,
                            iconUrl: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                        }
                    );
                });
                this.serviceLoaded = true;
            });
        }
    }

    getAreas() {
        this.areaService.getAreas().subscribe(resp => {
            this.areas = resp;
            return resp;
        });
    }

    async getAddress(lat: number, lng: number): Promise<any> {
        if (navigator.geolocation) {
            const geocoder = new google.maps.Geocoder();
            const latlng = new google.maps.LatLng(lat, lng);
            const request = {latLng: latlng};
            geocoder.geocode(request, (results, status) => {
                if (status === google.maps.GeocoderStatus.OK) {
                    while (results == null) {
                    }
                    const result = results[0];
                    this.address = result.formatted_address;
                    console.log(result.formatted_address);
                    return result.formatted_address;
                }
            });
        }
    }

    drawRouteToTheClosetsService() {
        const serviceUnit = this.findClosestService();
        this.drawRoute(this.location.coords, serviceUnit.address.coords);
    }

    findingClosestRoutePossible() {
        return this.serviceUnits.length !== 0;
    }

    findClosestService() {
        let serviceUnit = this.serviceUnits[0];
        this.serviceUnits.forEach(r => {
            if (this.calculateDistance(this.location.coords, r.address.coords)
                < this.calculateDistance(this.location.coords, serviceUnit.address.coords)) {
                serviceUnit = r;
            }
        });
        return serviceUnit;
    }


    calculateDistance(point1: CoordsDto, point2: CoordsDto) {
        const p1 = new google.maps.LatLng(
            Number(point1.latitude),
            Number(point1.longitude)
        );
        const p2 = new google.maps.LatLng(
            Number(point2.latitude),
            Number(point2.longitude)
        );
        return (
            google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000
        ).toFixed(2);
    }

    drawRoute(point1: CoordsDto, point2: CoordsDto) {
        this.origin = {
            lat: Number(point1.latitude),
            lng: Number(point1.longitude)
        };
        this.destination = {
            lat: Number(point2.latitude),
            lng: Number(point2.longitude)
        };
    }

    clearRoute() {
        this.origin = null;
        this.destination = null;
    }

    isRouteDrewOnMap() {
        return this.origin !== null && this.destination !== null;
    }

    addMarker(lat: number, lng: number) {
        // console.log(lat);
        //         // console.log(lng);
        //         // if (navigator.geolocation) {
        //         //     const geocoder = new google.maps.Geocoder();
        //         //     const latlng = new google.maps.LatLng(lat, lng);
        //         //     const request = {latLng: latlng};
        //         //     geocoder.geocode(request, (results, status) => {
        //         //         if (status === google.maps.GeocoderStatus.OK) {
        //         //             while (results == null) {
        //         //             }
        //         //             const result = results[0];
        //         //             this.address = result.formatted_address;
        //         //             console.log(results);
        //         //         }
        //         //     });
        //         // }
        this.location.coords = {
            latitude: lat,
            longitude: lng
        };
        this.location.markersToRender[0].lat = lat;
        this.location.markersToRender[0].lng = lng;
    }

    // selectMarker(event) {
    //     this.selectedMarker = {
    //         lat: event.latitude,
    //         lng: event.longitude,
    //         label: event.label;
    //         iconUrl: event
    //     };
    // }

    async setCurrentPosition() {
        if (navigator.geolocation) {
            await navigator.geolocation.getCurrentPosition(position => {
                //   console.log(position);
                const {longitude, latitude} = position.coords;
                this.defaultCords = position.coords;
                this.location = {
                    coords: {
                        latitude,
                        longitude
                    },
                    mapType: 'roadmap',
                    zoom: 14,
                    markersToRender: [
                        {
                            lat: latitude,
                            lng: longitude,
                            label: 'My current position',
                            iconUrl: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                        }]
                }
                ;
            });
        } else {
            alert('Geolocation is not supported by this browser, please use google chrome.');
        }
    }


    markerDragEnd($event: MouseEvent) {
        // this.location.latitude =
        // this.location.longitude = coords.longitude;
        console.log($event);
    }

}


export interface Marker {
    lat: number;
    lng: number;
    label: string;
    iconUrl: string;
}

interface Location {
    coords: CoordsDto;
    mapType: string;
    zoom: number;
    markersToRender: Array<Marker>;
}
