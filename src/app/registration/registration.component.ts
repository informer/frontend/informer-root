import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '@app/_services';
import {UserForm} from '@app/_models/userform';
import {UserRegisterForm} from '@app/_models/userRegisterForm';
import {MustMatch} from '@app/_helpers/mustmatch.validator';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.less']
})
export class RegistrationComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            phoneNumber: ['', [Validators.required, Validators.pattern('^[0-9]{9}')]]
        }, {validator: MustMatch('password', 'confirmPassword')});

        this.returnUrl = '/';
        this.route.queryParamMap.subscribe(param => {
            const token = param.get('token');
            if (token) {
                this.authenticationService.confirm(token).subscribe(resp => console.log(resp));
                this.router.navigate(['/login']);
            }
        });
    }

    get f() {
        return this.registerForm.controls;
    }

    onSubmit() {
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        const user = new UserRegisterForm(this.f.email.value, this.f.password.value,
            this.f.firstName.value, this.f.lastName.value, this.f.phoneNumber.value);
        console.log(user);
        this.authenticationService.register(user)
            .pipe()
            .subscribe(
                data => {
                    console.log(data);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }
}
