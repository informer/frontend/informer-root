import {Component, OnInit, ViewChild} from '@angular/core';
import {NotificationService} from '@app/_services/notification.service';
import {NotificationDto} from '@app/_models/dto';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.less']
})
export class NotificationsComponent implements OnInit {

    nameColumn: string[] = ['id', 'desc', 'incidentType', 'incidentTypeDesc'];
    notificationDataSource: MatTableDataSource<NotificationDto>;
    // @ts-ignore
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private notificationService: NotificationService) {
    }

    ngOnInit() {
      this.notificationDataSource = new MatTableDataSource<NotificationDto>([]);
      this.getNotifications();
    }

    isListContainingNotification() {
        return this.notificationDataSource.data.length !== 0;
    }

    getNotifications() {
        this.notificationService.getUserNotification().subscribe(
            resp => {
                console.log(resp);
                this.notificationDataSource = new MatTableDataSource<NotificationDto>(resp);
                this.notificationDataSource.paginator = this.paginator;
            });


    }


}
