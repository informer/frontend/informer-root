﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import {AppComponent} from './app.component';
import {appRoutingModule} from './app.routing';

import {JwtInterceptor, ErrorInterceptor} from './_helpers';
import {HomeComponent} from './home';
import {AdminComponent} from './admin';
import {LoginComponent} from './login';
import {JwtResolver} from '@app/_helpers/jwt.resolver';
import {RegistrationComponent} from './registration/registration.component';
import {MapComponent} from './map/map.component';
import {AgmCoreModule} from '@agm/core';
import {AgmDirectionModule} from 'agm-direction';
import {MapHelper} from '@app/_helpers/map.helper';
import {NgxSpinnerModule} from 'ngx-spinner';
import {
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDividerModule, MatExpansionModule,
    MatFormFieldModule, MatInputModule, MatListModule,
    MatOptionModule,
    MatSelectModule, MatSlideToggleModule, MatSortModule, MatTableModule
} from '@angular/material';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';


import {NotificationsComponent} from './notifications/notifications.component';
import {NotifyComponent} from './notify/notify.component';
import {AmplifyAngularModule, AmplifyService} from 'aws-amplify-angular';
import {NotificationListComponent} from './notification-list/notification-list.component';
import {ServicesComponent} from './services/services.component';
import {InctypesComponent} from './inctypes/inctypes.component';
import {NotificationComponent} from './notification/notification.component';
import {ServedNotificationsComponent} from './served-notifications/served-notifications.component';

declare const uploadingSpinnerName = 'uploading';
declare const setServiceSpinnerName = 'service';



@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBdm4nwPez1DYy1o27NS5hE0XU92G05Y6E',
            libraries: ['places', 'geometry']
        }),
        AgmDirectionModule,
        appRoutingModule,
        FormsModule,
        MatCardModule,
        MatDividerModule,
        MatFormFieldModule,
        BrowserAnimationsModule,
        MatOptionModule,
        MatSelectModule,
        NoopAnimationsModule,
        MatButtonToggleModule,
        MatButtonModule,
        MatInputModule,
        MatListModule,
        MatTableModule,
        MatSortModule,
        MatExpansionModule,
        MatSlideToggleModule,
        AmplifyAngularModule,
        NgxSpinnerModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        AdminComponent,
        LoginComponent,
        RegistrationComponent,
        MapComponent,
        NotificationsComponent,
        NotifyComponent,
        ServicesComponent,
        NotificationListComponent,
        InctypesComponent,
        NotificationComponent,
        ServedNotificationsComponent],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
        JwtResolver,
        MapHelper,
        AmplifyService
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
}
