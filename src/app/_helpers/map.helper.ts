import {Injectable} from '@angular/core';

declare let google: any;
@Injectable()
export class MapHelper {

    async setCurrentPosition(): Promise<any> {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                const coords: {longitude, latitude} = position.coords;
                return coords;
            });
        } else {
            alert('Geolocation is not supported by this browser, please use google chrome.');
        }
    }

    async getAddress( lat: number, lng: number ): Promise<any> {
        if (navigator.geolocation) {
            const geocoder = new google.maps.Geocoder();
            const latlng = new google.maps.LatLng(lat, lng);
            const request = { latLng: latlng };
            geocoder.geocode(request, (results, status) => {
                if (status === google.maps.GeocoderStatus.OK) {
                    while (results == null) {}
                    const result = results[0];
                    return result.formatted_address;
                }
            });
        }}

}
