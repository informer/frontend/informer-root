import {Component, OnInit} from '@angular/core';
import {AreaDto, CoordsDto, IncidentNotificationForm, IncidentTypeDto} from '@app/_models/dto';
import 'hammerjs';
import {Marker} from '@app/map/map.component';
import {NotificationService} from '@app/_services/notification.service';
import {FileS3Service} from '@app/_services/files.s3.service';
import {FormControl} from '@angular/forms';
import {IncidentTypeService} from '@app/_services/incident-type.service';
import {AreaService} from '@app/_services/area.service';
import swal from 'sweetalert2';
import {Router} from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {SpinnerEnum} from '@app/_helpers/spinner.enum';

declare let google: any;

@Component({
    selector: 'app-notify',
    templateUrl: './notify.component.html',
    styleUrls: ['./notify.component.less']
})
export class NotifyComponent implements OnInit {

    private incidentNotification: IncidentNotificationForm = new IncidentNotificationForm();
    private coords: CoordsDto;
    private mapCenter: CoordsDto;
    private address: string;
    private fromMap = false;
    private mapType = 'roadmap';
    private zoom = 13;
    private marker: Marker;
    private addressResult: any;
    private selectedFile: File;
    area: AreaDto;
    incidentTypesDto: any;
    incidentTypes = new FormControl();


    constructor(private  notificationService: NotificationService, private s3FileService: FileS3Service,
                private incidentTypeService: IncidentTypeService, private areaService: AreaService, private spinner: NgxSpinnerService) {
    }

    ngOnInit() {
        this.incidentNotification.areaId = 1;
        this.setCurrentPosition();
        this.getIncidentTypes();
        this.getArea();
    }

    onFileSelected(event: any) {
        this.selectedFile = event.target.files[0];
        this.incidentNotification.photoUrl = this.selectedFile.name;
    }

    notify() {
        if (this.address) {
            this.resolveAddress();
        }
        this.spinner.show(SpinnerEnum.uploading);
        this.s3FileService.upload(this.selectedFile, this.incidentNotification, this.spinner);
    }

    getArea() {
        this.areaService.getAreas().subscribe(
            resp => {
                this.area = resp[0];
            }
        );
    }

    resolveAddress() {
        this.incidentNotification.address = {
            city: this.addressResult.address_components[4].long_name,
            street: this.addressResult.address_components[1].long_name,
            buildingNumber: this.addressResult.address_components[0].long_name,
            coords: this.coords,
            areaId: this.area.id
        };
        this.incidentNotification.latitude = String(this.coords.latitude);
        this.incidentNotification.longitude = String(this.coords.longitude);

    }



    public getIncidentTypes() {
        return this.incidentTypeService.getIncidentTypes().subscribe(resp => {
            console.log(resp);
            this.incidentTypesDto = resp;
            return resp;
        });
    }

    public getType(type: IncidentTypeDto, event: any) {
        if (event.isUserInput) {
            this.incidentNotification.incidentTypeId = type.id;
        }
    }

    public loadAddress() {
        if (this.address === undefined) {
            this.getAddress();
            console.log(this.address);
        }
    }

    public addMarker(latitude: number, longitude: number) {
        console.log(latitude);
        console.log(longitude);
        this.coords = {
            latitude,
            longitude
        };
        this.marker = {
            lat: latitude,
            lng: longitude,
            label: '',
            iconUrl: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        };
        this.getAddress();
    }

    async setCurrentPosition() {
        if (navigator.geolocation) {
            await navigator.geolocation.getCurrentPosition(position => {
                const {longitude, latitude} = position.coords;
                this.coords = {
                    latitude,
                    longitude
                };
                this.mapCenter = {
                    latitude,
                    longitude
                };
                this.marker = {
                    lat: latitude,
                    lng: longitude,
                    label: '',
                    iconUrl: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };
            });
        } else {
            alert('Geolocation is not supported by this browser, please use google chrome.');
        }
    }

    async getAddress(): Promise<any> {
        if (navigator.geolocation && this.coords.longitude && this.coords.latitude) {
            const geocoder = new google.maps.Geocoder();
            const latlng = new google.maps.LatLng(this.coords.latitude, this.coords.longitude);
            const request = {latLng: latlng};
            geocoder.geocode(request, (results, status) => {
                if (status === google.maps.GeocoderStatus.OK) {
                    while (results == null) {
                    }
                    const result = results[0];
                    this.addressResult = result;
                    this.address = result.formatted_address;
                    return result.formatted_address;
                }
            });
        }
    }
}
