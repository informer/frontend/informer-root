import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InctypesComponent } from './inctypes.component';

describe('InctypesComponent', () => {
  let component: InctypesComponent;
  let fixture: ComponentFixture<InctypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InctypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InctypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
