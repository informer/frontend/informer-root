import {Component, OnInit} from '@angular/core';
import {
    IncidentTypeAddForm,
    IncidentTypeDto,
    IncidentTypeServiceAssignation,
    IncidentTypesWithServicesDto,
    NotificationDto,
    ServiceUnitDto
} from '@app/_models/dto';
import {IncidentTypeService} from '@app/_services/incident-type.service';
import {FormControl} from '@angular/forms';
import {PublicServicesService} from '@app/_services/publicservices.service';
import {MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';

@Component({
    selector: 'app-inctypes',
    templateUrl: './inctypes.component.html',
    styleUrls: ['./inctypes.component.less']
})
export class InctypesComponent implements OnInit {

    incidentType: IncidentTypeAddForm = new IncidentTypeAddForm();
    incidentAssignation: IncidentTypeServiceAssignation = new IncidentTypeServiceAssignation();
    incidentTypesDto = [];
    incidentTypesWithServiceDto: any;
    incidentTypes = new FormControl();
    servicesListDto: any;
    services = new FormControl();
    nameColumn: string[] = ['id', 'name', 'desc', 'serviceName'];
    incidentTypesWithServicesDataSource: MatTableDataSource<IncidentTypesWithServicesDto>;

    constructor(private incidentTypeService: IncidentTypeService,
                private publicServicesService: PublicServicesService,
                private router: Router) {
    }

    ngOnInit() {
        this.incidentTypesWithServicesDataSource = new MatTableDataSource<IncidentTypesWithServicesDto>([]);
        this.getService();
        this.getIncidentTypesWithServices();
    }

    public getIncidentTypesWithServices() {
        return this.incidentTypeService.getExtendedIncidentType().subscribe(resp => {
            this.incidentTypesWithServiceDto = resp;
            resp.forEach(r => {
                if (r.services.length === 0) {
                    this.incidentTypesDto.push(r);
                }
            });
            this.incidentTypesWithServicesDataSource = new MatTableDataSource<IncidentTypesWithServicesDto>(resp);
            console.log(resp);
            return resp;
        });
    }

    getService() {
        this.publicServicesService.getServices().subscribe(resp => {
            this.servicesListDto = resp;
            return resp;
        });
    }

    public getTypeFromSelect(type: IncidentTypeDto, event: any) {
        if (event.isUserInput) {
            this.incidentAssignation.incidentTypeId = type.id;
        }
    }

    public getServiceFromSelect(service: ServiceUnitDto, event: any) {
        if (event.isUserInput) {
            console.log(service);
            console.log(this.incidentAssignation);
            this.incidentAssignation.serviceId = service.id;
        }
    }

    addIncidentType() {
        this.incidentTypeService.addIncidentType(this.incidentType).subscribe(resp => {
            console.log(resp);
            this.ngOnInit();
        });
    }

    assign() {
        console.log(this.incidentAssignation);
        this.incidentTypeService.assignTypeWithService(this.incidentAssignation).subscribe(resp => {
            console.log(resp);
            this.ngOnInit();
        });
    }
}
