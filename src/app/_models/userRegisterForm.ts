export class UserRegisterForm {
      email: string;
      password: string;
      firstName: string;
      lastName: string;
      phoneNumber: string;

    constructor(email: string, password: string, firstName: string, lastName: string, phoneNumber: string) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }
}
