export class Userinfo {

    constructor(firstName: string, lastName: string, email: string, phoneNumber: string, currentLocation: string) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.currentLocation = currentLocation;
    }
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    currentLocation: string;
}
