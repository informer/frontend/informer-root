﻿import { Role } from './role';

export class UserToken {
    role: Role;
    token?: string;

    constructor(role: Role, token: string) {
        this.role = role;
        this.token = token;
    }
}
