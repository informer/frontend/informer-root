// Generated using typescript-generator version 2.0.400 on 2019-12-08 16:07:12.

export interface IncidentTypeDto {
    id: number;
    name: string;
    description: string;
}

export interface AddressForm {
    city: string;
    street: string;
    buildingNumber: string;
    areaId: number;
    coords: CoordsDto;
}

export interface AdminRegisterForm {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
}

export class PublicServiceUnitAddForm {
    name: string;
    email: string;
    phoneNumber: string;
    publicServiceId: number;
    address: AddressForm;
}

export interface ChangePasswordForm {
    id: number;
    oldPassword: string;
    newPassword: string;
}

export interface DispatcherRegisterForm {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
}

export class IncidentTypeAddForm {
    name: string;
    description: string;
}

export interface IncidentTypesWithServicesDto {
    id: number;
    name: string;
    description: string;
    services: PublicServiceDto[];
}

export class IncidentTypeServiceAssignation {
    serviceId: number;
    incidentTypeId: number;
}

export class IncidentNotificationForm {
    description: string;
    latitude: string;
    longitude: string;
    photoUrl: string;
    incidentTypeId: number;
    areaId: number;
    address: AddressForm;
}

export class PublicServiceAddForm {
    name: string;
    description: string;
}

export interface RegularUserRegisterForm {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
}

export interface UserRegisterForm {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
}

export interface ServiceUnitAssignationToIncident {
    notificationId: number;
    serviceUnitId: number;
}

export interface AddressDto {
    id: number;
    city: string;
    street: string;
    buildingNumber: string;
    area: AreaDto;
    coords: CoordsDto;
}

export interface AreaDto {
    id: number;
    city: string;
    centralPoint: CoordsDto;
}

export interface CoordsDto {
    latitude: number;
    longitude: number;
}

export interface NotificationDto {
    id: number;
    incidentType: IncidentTypeDto;
    description: string;
    isConfirmed: boolean;
    addressDto: AddressDto;
    photoUrl: string;
    area: AreaDto;
}

export interface PublicServiceDto {
    id: number;
    name: string;
    description: string;
}

export interface RegisteredUser {
    email: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
}

export interface ExpandedNotificationDto {
    description: string;
    photoUrl: string;
    areaId: number;
    address: AddressDto;
    incidentTypeDto: IncidentTypeDto;
    user: RegisteredUser;
    publicServiceDtoList: PublicServiceDto[];
}

export interface ServiceUnitDto {
    id: number;
    name: string;
    publicServiceId: number;
    publicService: string;
    address: AddressDto;
}
