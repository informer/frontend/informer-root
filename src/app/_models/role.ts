﻿export enum Role {
    Regular = 'ROLE_REGULAR',
    Dispatcher = 'ROLE_DISPATCHER'
}
