﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { UserToken, Role } from './_models';

// tslint:disable-next-line:component-selector
@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
    userToken: string;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.userToken.subscribe(x => this.userToken = x);
    }

    get isAdmin() {
        return this.userToken && localStorage.getItem('ROLE') === 'ROLE_ADMIN';
    }

    get isDispatcher() {
        return this.userToken && localStorage.getItem('ROLE') === 'ROLE_DISPATCHER';
    }

    get isRegularUser() {
        return this.userToken && localStorage.getItem('ROLE') === 'ROLE_REGULAR';
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}
