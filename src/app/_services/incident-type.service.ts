import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IncidentTypeAddForm, IncidentTypeDto, IncidentTypeServiceAssignation, IncidentTypesWithServicesDto} from '@app/_models/dto';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class IncidentTypeService {
    constructor(private http: HttpClient) {
    }


    public getIncidentTypes() {
        return this.http.get<Array<IncidentTypeDto>>('http://localhost:8080/incident-types')
            .pipe(map(resp => {
                return resp;
            }));
    }


    public getExtendedIncidentType() {
        return this.http.get<Array<IncidentTypesWithServicesDto>>('http://localhost:8080/incident-types/with-service')
            .pipe(map(resp => {
                return resp;
            }));
    }

    public addIncidentType(incidentTypeForm: IncidentTypeAddForm) {
        return this.http.post<IncidentTypeAddForm>('http://localhost:8080/incident-types', incidentTypeForm)
            .pipe(map(resp => {
                return resp;
            }));
    }

    public assignTypeWithService(assignation: IncidentTypeServiceAssignation) {
        return this.http.post<IncidentTypeServiceAssignation>('http://localhost:8080/incident-types/assign-service', assignation)
            .pipe(map(resp => {
                return resp;
            }));
    }
}
