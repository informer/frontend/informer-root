﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Role, UserToken} from '@app/_models';
import {UserForm} from '@app/_models/userform';
import {JwtResolver} from '@app/_helpers/jwt.resolver';
import {UserRegisterForm} from '@app/_models/userRegisterForm';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<string>;
    public userToken: Observable<string>;

    constructor(private http: HttpClient, private resolver: JwtResolver) {
        this.currentUserSubject = new BehaviorSubject<string>(localStorage.getItem('Authorization'));
        this.userToken = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): string {
        return this.currentUserSubject.value;
    }

    public get currentUserRole(): string {
        return this.resolver.getDecodedAccessToken(localStorage.getItem('Authorization')).role[0];
    }

    login(userForm: UserForm) {
        return this.http.post<any>('http://localhost:8080/login', {username: userForm.username, password: userForm.password})
            .pipe(map(resp => {
                if (resp.Authorization && resp) {
                    const token = resp.Authorization.split(' ', 2)[1];
                    localStorage.setItem('Authorization', JSON.stringify(token));
                    localStorage.setItem('ROLE', this.resolver.getDecodedAccessToken(resp.Authorization).role[0]);
                    return new UserToken(this.resolver.getDecodedAccessToken(resp.Authorization).role[0], resp.Authorization);
                }
                return resp;
            }));
    }


    register(userRegisterForm: UserRegisterForm) {
        return this.http.post<any>('http://localhost:8080/register', userRegisterForm)
            .pipe(map(resp => {
                console.log(resp);
                return resp;
            }));
    }

    confirm(token: string) {
        return this.http.get<any>('http://localhost:8080/register/confirm?token=' + token, )
            .pipe(map(resp => {
                console.log(resp);
                return resp;
            }));
    }

    logout() {
        localStorage.removeItem('Authorization');
        this.currentUserSubject.next(null);
    }
}
