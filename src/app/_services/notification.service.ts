import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {
    ExpandedNotificationDto,
    IncidentNotificationForm,
    IncidentTypeDto,
    NotificationDto,
    ServiceUnitAssignationToIncident
} from '@app/_models/dto';

@Injectable({providedIn: 'root'})
export class NotificationService {
    constructor(private http: HttpClient) {
    }

    public getUserNotification() {
        return this.http.get<Array<NotificationDto>>('http://localhost:8080/notifications/my-notifications')
            .pipe(map(resp => {
                return resp;
            }));
    }

    public getNotification(id: number) {
        return this.http.get<ExpandedNotificationDto>('http://localhost:8080/notifications/' + id)
            .pipe(map(resp => {
                return resp;
            }));
    }

    public getAllUnservicedNotification() {
        return this.http.get<Array<NotificationDto>>('http://localhost:8080/notifications/unserviced')
            .pipe(map(resp => {
                return resp;
            }));
    }

    public getAllServicedNotification() {
        return this.http.get<Array<NotificationDto>>('http://localhost:8080/notifications/serviced')
            .pipe(map(resp => {
                return resp;
            }));
    }

    public sendNotification(incidentNotification: IncidentNotificationForm) {
        return this.http.post<IncidentNotificationForm>('http://localhost:8080/notifications', incidentNotification)
            .pipe(map(resp => {
                console.log(resp);
                return resp;
            }));
    }


    assignNotificationToUnit(buildAssignationDto: ServiceUnitAssignationToIncident) {
        return this.http.post<ServiceUnitAssignationToIncident>('http://localhost:8080/notifications/assign-service', buildAssignationDto)
            .pipe(map(resp => {
                console.log(resp);
                return resp;
            }));

    }
}
