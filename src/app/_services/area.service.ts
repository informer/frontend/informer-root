import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {AreaDto} from '@app/_models/dto';

@Injectable({providedIn: 'root'})
export class AreaService {
    constructor(private http: HttpClient) {
    }

    public getAreas() {
        return this.http.get<Array<AreaDto>>('http://localhost:8080/areas')
            .pipe(map(resp => {
                return resp;
            }));
    }

}
