import {Injectable} from '@angular/core';
import {Auth, Storage} from 'aws-amplify';
import {Observable, Subject} from 'rxjs';
import {IncidentNotificationForm, NotificationDto} from '@app/_models/dto';
import {NotificationService} from '@app/_services/notification.service';
import swal from 'sweetalert2';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {SpinnerEnum} from '@app/_helpers/spinner.enum';

@Injectable({
    providedIn: 'root'
})
export class FileS3Service {

    constructor(private notificationService: NotificationService, private router: Router) {
    }

    public async getImage(path: string, res: string): Promise<string> {
         return await Storage.get(path).then(result => {
            res = result.toString();
            return result.toString();
        }).catch(err => {
            return null;
        });
    }


    async delete(path: string) {
        await Storage.remove(path).then(
            result => {
                console.log('REMOVED FILE: ' + path);
            }).catch(
            error => {
                console.log('ERROR DURING REMOVING FILE" ' + path);
            });
    }

    download(path: string) {
        Storage.get(path).then(result => {
            console.log(result);
            window.open(result.toString());
        }).catch(err => {
            console.log(err);
        });
    }

    async upload(file: File, incidentNotification: IncidentNotificationForm, spinner: NgxSpinnerService) {
        await Storage.put(file.name, file, {
                progressCallback(progress) {
                    // todo progress message
                }
            }
        ).then(result => {
            // @ts-ignore
            incidentNotification.photoUrl = result.key;
            this.notificationService.sendNotification(incidentNotification).subscribe(resp => console.log(resp));
            spinner.hide(SpinnerEnum.uploading);
            swal.fire('Zgłoszenie wysłane', 'Twoje zgłoszenie zostało wysłane', 'success').then(r => {
                this.router.navigate(['/']);
            });
        }).catch(err => {
            console.log('error');
            console.log(err);
            swal.fire('Błąd', 'W trakcie zgłoszenia coś poszło nie tak ', 'error').then(r => {
                this.router.navigate(['/']);
            });
        });
        // @ts-ignore

    }

}
