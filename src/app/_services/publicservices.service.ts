import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {PublicServiceAddForm, PublicServiceDto, PublicServiceUnitAddForm, ServiceUnitDto} from '@app/_models/dto';

@Injectable({providedIn: 'root'})
export class PublicServicesService {
    constructor(private http: HttpClient) {
    }

    public getServices() {
        return this.http.get<Array<PublicServiceDto>>('http://localhost:8080/services')
            .pipe(map(resp => {
                return resp;
            }));
    }

    public getServiceUnits(areaId: number, serviceId: number) {
        return this.http.get<Array<ServiceUnitDto>>('http://localhost:8080/services/service-unit?area=' + areaId + '&service=' + serviceId)
            .pipe(map(resp => {
                return resp;
            }));
    }

    public addPublicService(form: PublicServiceAddForm) {
        return this.http.post<PublicServiceAddForm>('http://localhost:8080/services', form)
            .pipe(map(resp => {
                return resp;
            }));
    }

    public addPublicServiceUnit(form: PublicServiceUnitAddForm) {
        return this.http.post<PublicServiceUnitAddForm>('http://localhost:8080/services/unit', form)
            .pipe(map(resp => {
                return resp;
            }));
    }

}
