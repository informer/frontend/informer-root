
export interface S3Service {
    progressS3(message: ProgressMessage): void;
    // startUploading(idEvent: number, path: string, size: number): Promise<VideoCreationDtoOutput>;
    endUploading(idVideo: number);
    endUploadingAll();
}

export interface S3ServiceDeleted {
    deleted(): void;
}

export class ProgressMessage {
    nameCurrentlyUploadingFile: string;
    sizeUploadedFile: number;
    sizeAllFile: number;
    numberUploadingFile: number;
    numberAllFile: number;
    fileError: File[];
    progress: string;


    constructor() {
        this.numberAllFile = 0;
        this.numberUploadingFile = 1;
        this.fileError = [];
        this.sizeUploadedFile = 0;
        this.sizeAllFile = 0;
    }
}
