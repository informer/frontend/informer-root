import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CoordsDto, ExpandedNotificationDto, PublicServiceDto, ServiceUnitAssignationToIncident, ServiceUnitDto} from '@app/_models/dto';
import {NotificationService} from '@app/_services/notification.service';
import {FileS3Service} from '@app/_services/files.s3.service';
import {PublicServicesService} from '@app/_services/publicservices.service';
import swal from 'sweetalert2';
import {NgxSpinnerService} from 'ngx-spinner';
import {SpinnerEnum} from '@app/_helpers/spinner.enum';


declare let google: any;

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.less']
})
export class NotificationComponent implements OnInit {

    location: Location;
    notificationId: number;
    expandedNotificationDto: ExpandedNotificationDto;
    assignation: ServiceUnitAssignationToIncident;
    url: string;
    serviceUnits: Array<ServiceUnitDto> = new Array<ServiceUnitDto>();


    constructor(private route: ActivatedRoute, private notificationService: NotificationService,
                private s3FileService: FileS3Service, private publicServicesService: PublicServicesService,
                private router: Router, private spinner: NgxSpinnerService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.notificationId = params.idNotification;
        });
        this.getNotification();
        console.log(this.serviceUnits);
    }

    getNotification() {
        this.notificationService.getNotification(this.notificationId).subscribe(resp => {
            console.log(resp);
            this.expandedNotificationDto = resp;
            console.log(this.expandedNotificationDto);
            this.getImage();
            this.getServiceUnits(this.expandedNotificationDto.publicServiceDtoList[0]);
            console.log(this.serviceUnits);

        });
    }

    getImage() {
        const str = this.s3FileService.getImage(this.expandedNotificationDto.photoUrl, this.url).then(r => {
            this.url = r;
        });
    }

    getServiceUnits(service: any) {
        this.publicServicesService.getServiceUnits(1, service.id).subscribe(resp => {
            this.serviceUnits = resp;
        });
    }

    findClosestService() {
        let serviceUnit = this.serviceUnits[0];
        this.serviceUnits.forEach(r => {
            if (this.calculateDistance(this.expandedNotificationDto.address.coords, r.address.coords)
                < this.calculateDistance(this.expandedNotificationDto.address.coords, serviceUnit.address.coords)) {
                serviceUnit = r;
            }
        });
        return serviceUnit;
    }


    calculateDistance(point1: CoordsDto, point2: CoordsDto) {
        const p1 = new google.maps.LatLng(
            Number(point1.latitude),
            Number(point1.longitude)
        );
        const p2 = new google.maps.LatLng(
            Number(point2.latitude),
            Number(point2.longitude)
        );
        return (
            google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000
        ).toFixed(2);
    }


    assignUnit() {
        if (!this.serviceUnits || this.serviceUnits.length === 0) {
            swal.fire('Nie istnieje żadna jednostka', 'Nie istnieje żadna jednostka zdolna obsłużyć to zgłoszenie', 'error').then(r => {
            });
        } else {
            this.spinner.show(SpinnerEnum.service);
            this.notificationService.assignNotificationToUnit(this.buildAssignationDto()).subscribe(resp => {
                this.spinner.hide(SpinnerEnum.service);
                swal.fire('Wysłano powiadomienie', 'Wysłano powiadomienie do najbliższej jednostki', 'success').then(r => {
                this.router.navigate(['/notification-list']);
              });
            });
        }
    }

    private buildAssignationDto() {
        return this.assignation = {
            notificationId: this.notificationId,
            serviceUnitId: this.findClosestService().id
        };
    }
}
