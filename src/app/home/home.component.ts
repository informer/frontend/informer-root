﻿import {Component, Input, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';

import {UserToken} from '@app/_models';
import {AuthenticationService} from '@app/_services';
import {MapHelper} from '@app/_helpers/map.helper';
import {Router} from '@angular/router';
import {Userinfo} from '@app/_models/userinfo';
import {FormControl} from '@angular/forms';

declare let google: any;

@Component({templateUrl: 'home.component.html'})
export class HomeComponent implements OnInit {
    location: Location;
    loading = false;
    userToken: string;
    userFromApi: UserToken;
    user: Userinfo;
    findPath = false;

    services = new FormControl();
    serviceList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];


    constructor(
        private authenticationService: AuthenticationService,
        private mapHelper: MapHelper,
        private router: Router
    ) {
        this.userToken = this.authenticationService.currentUserValue;
    }

    // tslint:disable-next-line:use-lifecycle-interface
    ngOnInit() {
        this.loading = true;
        this.loading = false;
        this.setCurrentPosition();
        this.router.navigate(['/']);
    }
    setCurrentPosition() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                console.log(position);
                const {longitude, latitude} = position.coords;
                this.location = {
                    latitude,
                    longitude,
                    mapType: 'roadmap',
                    zoom: 14,
                    marker:
                        {
                            lat: latitude,
                            lng: longitude,
                            label: 'Twoje polożenie'
                        }
                };
            });
        } else {
            alert('Geolocation is not supported by this browser, please use google chrome.');
        }
    }

    // async getAddress() {
    //     if (navigator.geolocation) {
    //         navigator.geolocation.getCurrentPosition(position => {
    //         const geocoder = new google.maps.Geocoder();
    //         const latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    //         const request = { latLng: latlng };
    //         geocoder.geocode(request, (results, status) => {
    //             if (status === google.maps.GeocoderStatus.OK) {
    //                 while (results == null) {}
    //                 const result = results[0];
    //                 this.user.currentLocation = result.formatted_address;
    //                 console.log(this.address);
    //                 return result.formatted_address;
    //             }
    //         });
    //         });
    //     }}
}

interface Location {
    latitude: number;
    longitude: number;
    mapType: string;
    zoom: number;
    marker: Marker;
}


interface Marker {
    lat: number;
    lng: number;
    label: string;
}
