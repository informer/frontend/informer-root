import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {NotificationDto} from '@app/_models/dto';
import {NotificationService} from '@app/_services/notification.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-notification-list',
    templateUrl: './notification-list.component.html',
    styleUrls: ['./notification-list.component.less']
})
export class NotificationListComponent implements OnInit {

    nameColumn: string[] = ['id', 'desc', 'incidentType', 'incidentTypeDesc', 'more'];
    notificationDataSource: MatTableDataSource<NotificationDto>;
    // @ts-ignore
    @ViewChild(MatPaginator) paginator: MatPaginator;


    constructor(private notificationService: NotificationService, private router: Router) {
    }

    ngOnInit() {
        this.notificationDataSource = new MatTableDataSource<NotificationDto>([]);
        this.getNotifications();
    }

    isListContainingNotification() {
        return this.notificationDataSource.data.length !== 0;
    }

    getNotifications() {
        this.notificationService.getAllUnservicedNotification().subscribe(
            resp => {
                console.log(resp);
                this.notificationDataSource = new MatTableDataSource<NotificationDto>(resp);
                this.notificationDataSource.paginator = this.paginator;
            });


    }

    loadMore(id: number) {
        this.router.navigate(['notification-list/' + id]);
    }
}
