﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { UserToken } from '@app/_models';

@Component({ templateUrl: 'admin.component.html' })
export class AdminComponent {
    loading = false;
    users: UserToken[] = [];

    constructor() { }

    // ngOnInit() {
    //     this.loading = true;
    //     this.userService.getAll().pipe(first()).subscribe(users => {
    //         this.loading = false;
    //         this.users = users;
    //     });
    // }
}
